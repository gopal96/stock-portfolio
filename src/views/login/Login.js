import React, { useState } from 'react';
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CImg,
  CInputGroup,
  CRow
} from '@coreui/react'
import authService from '../../services/auth'
import {toast} from 'react-toastify'; 
import 'react-toastify/dist/ReactToastify.css'; 
import { getUser,setUserSession } from '../../Utils/Common';
const Keys = {
  USER_EMAIL:"USER_EMAIL",
  USER_PASSWORD:"USER_PASSWORD"
}
const Formdata = {
  USER_EMAIL:"",
  USER_PASSWORD:""
}

const Login = (props) => {
  toast.configure() 
  const user = getUser();
  if(user!=null){
    props.history.push('/dashboard');
  } 
  const [formData, setFormData] = useState(Formdata);
  const [loading, setLoading] = useState(false);
  const handleChange = (event) => {
    const { target } = event;
    const value = target.value;
    const check = target.checked
    const { name } = target;
    if(target.type === "checkbox"){
        if (check) {
            setFormData({ ...formData, [name]: parseInt(value) });
        }else{
            setFormData({ ...formData, [name]: 0 });
        }
    }else{
        setFormData({ ...formData, [name]: value });
    }
  };
// this function call when submit login form
  function handleLogin(event) {
    event.preventDefault()
    setLoading(true);
    authService
    .login(formData)
    .then((response) => {
        if(response.code===200){
          toast.success(response.msg);
          setUserSession(response.data);
          props.history.push('/dashboard');
        }else{
          toast.error(response.msg);
        }
        setLoading(false);
    })
    .catch((err) => {
        console.log(err);
        setLoading(false);
    });
  }

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CForm onSubmit={handleLogin}>
          <CRow className="justify-content-center">
            <CCol md="6">
              <CCardGroup>
                <CCard className="p-4">
                  <CCardBody>
                  <CImg
              className="d-block w-100"
              src="slider/slider-image-1.jpg"
              alt="Third slide"
            />
            
                      <p className="text-muted mt-2">Sign In to your account</p>
                      <CInputGroup className="mb-3">
                   
                        <CInput 
                          type="email" 
                          name={Keys.USER_EMAIL}
                          id={Keys.USER_EMAIL}
                          value={formData[Keys.USER_EMAIL]}
                          onChange={(event) => {
                              handleChange(event);
                          }}
                          onKeyUp={(event) => {
                              handleChange(event);
                          }}
                          placeholder="Email" 
                          autoComplete="email"
                          required
                          />
                      </CInputGroup>
                      <CInputGroup className="mb-4">
                     
                        <CInput 
                          type="password" 
                          name={Keys.USER_PASSWORD}
                          id={Keys.USER_PASSWORD}
                          value={formData[Keys.USER_PASSWORD]}
                          onChange={(event) => {
                              handleChange(event);
                          }}
                          onKeyUp={(event) => {
                              handleChange(event);
                          }}
                          placeholder="Password" 
                          autoComplete="password" 
                          required
                          />
                      </CInputGroup>
                      <CRow>
                        <CCol>
                          <CButton type="submit" disabled={loading} color="warning" className="px-4">{loading ? 'Please Wait...' : 'Login'}</CButton>
                        </CCol>
                      </CRow>
                    
                  </CCardBody>
                </CCard>
                
              </CCardGroup>
            </CCol>
          </CRow>
        </CForm>                        
      </CContainer>
    </div>
  )
}

export default Login
