import React, { lazy, useState, Suspense } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow

} from '@coreui/react'
import wishList from '../../services/wishList'
const WidgetsDropdown = lazy(() => import('../widgets/WidgetsDropdown.js'));
const AddStockModal = React.lazy(() => import("../modals/AddStockModal"));
const MyPortfolioModal = React.lazy(() => import("../modals/MyPortfolioModal"));

const Dashboard = () => {
  const [buyShareModal, setBuyShareModal] = useState(false);
  const [selectedShareId, setSelectedShareId] = useState();
  const [chartModal, setChartModal] = useState();
  const toggle = () => {
    setBuyShareModal(!buyShareModal);
  };
  const viewToggle = () => {
    setChartModal(!chartModal);
  };

  const showBuyModel = (e, shareid, btn) => {
    setSelectedShareId(shareid);
    if (btn === "buy") {
      toggle();
    } else if (btn === "view") {
      viewToggle();
    }
  };

  return (

    <>
   
      <WidgetsDropdown />
      <CRow>
        <CCol>
          <CCard>
            <CCardHeader>
              Watchlist
            </CCardHeader>
            <CCardBody>
              <table className="table table-hover table-outline mb-0 d-none d-sm-table">
                <thead className="thead-light">
                  <tr>

                    <th className="text-center">Scrip name</th>
                    <th className="text-center">LTP</th>
                    <th className="text-center">Chg</th>
                    <th className="text-center">Chg(%)</th>
                    <th className="text-center">ACTION</th>

                  </tr>
                </thead>
                <tbody>
                  {wishList.length ?
                    wishList.map((share, index) => (
                      <tr key={index}>

                        <td className="text-center">
                          <div>{share.SCRIP_NAME}</div>
                          <div className="small text-muted">
                            {share.SCRIP_FULL_NAME}
                          </div>
                        </td>
                        <td className="text-center">
                          <div>{share.LTP}</div>

                        </td>
                        <td className="text-center">
                          <div className="clearfix">
                            <div className="">
                              <strong>{share.CHG}</strong>
                            </div>
                          </div>
                        </td>
                        <td className="text-center">
                          <div className="clearfix">
                            <div className="">
                              <strong> {share.CHG_PER} </strong>
                            </div>
                          </div>
                        </td>
                        <td className="text-center">
                          <div className="clearfix">
                            <CButton
                              color="primary"
                              onClick={(e) => showBuyModel(e, share.ID, "buy")}
                              className={'mb-1 mr-3'}
                            >BUY</CButton>
                            <CButton
                              color="success"
                              onClick={(e) => showBuyModel(e, share.ID, "view")}
                              className={'mb-1'}
                            >VIEW</CButton>

                          </div>
                        </td>
                      </tr>
                    )) : (<>DATA NOT AVAILABLE</>)}
                </tbody>
              </table>

            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <Suspense fallback={<div>LOADING</div>}>
        <AddStockModal
          isOpen={buyShareModal}
          toggleModal={() => toggle()}
          shareid={selectedShareId}
        />
        <MyPortfolioModal
          isOpen={chartModal}
          toggleModal={() => viewToggle()}
          shareid={selectedShareId}
        />
      </Suspense>
    </>
  )
}

export default Dashboard
