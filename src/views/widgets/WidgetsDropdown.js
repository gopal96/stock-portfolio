import React from 'react'
import {
  CRow,
  CCol,
  CCard,
  CCardBody,
  CCardHeader,

} from '@coreui/react'
const WidgetsDropdown = () => {
  // render
  return (
    <>
    <CRow>
    <CCol>
      <CCard>
      <CCardHeader>
      <div className='text-dark'>NIFTY</div><div id="dialog1" className="triangle_top"></div><div className='text-success'>17,352.45</div>
            </CCardHeader>
        <CCardBody><div className='text-success'>509.65(3.03%)</div></CCardBody>
      </CCard>
    </CCol>
    <CCol>
      <CCard>
      <CCardHeader>
      <div className='text-dark'>BSE SENSEX</div><div id="dialog1" className="triangle_top"></div><div className='text-success'>58,142.05</div>
            </CCardHeader>
        <CCardBody><div className='text-success'>736.21(3.08%)</div></CCardBody>
      </CCard>
    </CCol>
    <CCol>
      <CCard>
      <CCardHeader>
      <div className='text-dark'>BANKNIFTY</div><div id="dialog1" className="triangle_top"></div><div className='text-success'>38,170.10 </div>
            </CCardHeader>
        <CCardBody><div className='text-success'>261.55(3.42%)</div></CCardBody>
      </CCard>
    </CCol>
    <CCol>
      <CCard>
      <CCardHeader>
      <div className='text-dark'>INDIAVIX 
          </div><div id="dialog1" className="triangle_down"></div><div className='text-success'>20.61</div>
            </CCardHeader>
        <CCardBody><div className='text-danger'>-2.37(-10.31%)</div></CCardBody>
      </CCard>
    </CCol>

  </CRow>
  </>
  )
}

export default WidgetsDropdown
