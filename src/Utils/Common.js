// return the user data from the session storage
export const getUser = () => {
    const userStr = localStorage.getItem('user');
    if (userStr) return JSON.parse(userStr);
    else return null;
}

   
  // remove the token and user from the session storage
  export const removeUserSession = () => {
    localStorage.removeItem('user');
    localStorage.removeItem('portfolio');
  }
   
  // set the token and user from the session storage
  export const setUserSession = ( user) => {
    localStorage.setItem('user', JSON.stringify(user));
  }
  // set the portfolio data in locally
  export const setUserSessionPortfolio = ( data) => {
    localStorage.setItem('portfolio', JSON.stringify(data));
  }
  export const getPortfolioData = () => {
    const userStr = localStorage.getItem('portfolio');
    if (userStr) return JSON.parse(userStr);
    else return null;
}