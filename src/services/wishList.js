const wishList = [
    {
        ID: "1",
        SCRIP_NAME: "ICICIBANK",
        SCRIP_FULL_NAME: "ICICI BANK LTD",
        LTP: "776.05",
        CHG: "22.35",
        CHG_PER: "2.97",
    },
    {
        ID: "2",
        SCRIP_NAME: "INFY",
        SCRIP_FULL_NAME: "INFOSYS LTD",
        LTP: "1776.05",
        CHG: "56.35",
        CHG_PER: "3.33",
    },
    {
        ID: "3",
        SCRIP_NAME: "TCS",
        SCRIP_FULL_NAME: "TATA CONSULTANCY SERV LTD",
        LTP: "3817.05",
        CHG: "79.35",
        CHG_PER: "3.97",
    },
    {
        ID: "4",
        SCRIP_NAME: "BAJAJFINANCE",
        SCRIP_FULL_NAME: "BAJAJ FINANCE LIMITED",
        LTP: "7142.05",
        CHG: "352.35",
        CHG_PER: "5.25",
    },



]

export default wishList;
